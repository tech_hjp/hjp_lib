package in.teze.widget;

import android.app.Dialog;
import android.content.Context;

/**功能：
 * TzDialog
 * @author   by fooyou 2014年7月29日   上午9:40:39
 */
public class TzDialog extends Dialog{
	protected int animResId=R.style.Animations_Dialog_DropDown;

	public TzDialog(Context context) {
		this(context,R.style.CustomTheme_Dialog);
	}

	public TzDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		init();
	}

	public TzDialog(Context context, int theme) {
		super(context, theme);
		init();
	}
	
	private void init(){
		/*setCanceledOnTouchOutside(false);*/
		/*getWindow().setWindowAnimations(animResId);*/
	}

}
