package in.teze.widget;

import android.app.Activity;
import android.os.Bundle;

import android.view.Menu;

/**
 * @description for UI test
 * @author   by liaofuyong 
 * @date 2014年12月3日   下午5:25:03
 */
public class TestActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

}
